import json
def html2json(html):
    c = 0
    htmlArr = []
    tagIndex = 0
    isTagName = False
    attrIndex = 0
    isAttr = False
    attrValue = False
    attrInitChar = False
    attrInitChars = ['"',"'"]
    children = False
    textContent = False
    docType = False
    isComment = False
    lenHtml = len(html)
    while(c < lenHtml):
        char = html[c]
        if(isComment != True):
            if(char == '<' and html[c+1] != "/"):
                if(html[c+1] == "!" and html[c+2] == "-" and html[c+3] == "-"):
                    isComment = True
                    c+= 4
                    continue
                elif(html[c+1] == "!"):
                    docType = True
                else:
                    isTagName = True
                    htmlArr.append({"tagName":""})
                    if(textContent):
                        textContent = False
                        tagIndex +=1
            elif (char == '<' and html[c+1] == "/"):
                return {"content":htmlArr,"jump": c}
            elif (char == "/" and html[c+1] == ">"):
                tagIndex+=1
                isTagName = False
                isAttr = False
                attrIndex = 0
                c += 2
                continue
            else:
                if(char != ">"):
                    if(docType != True):
                        if(char == ' ' and isTagName and attrValue != True):
                            if(len(htmlArr[tagIndex]["tagName"]) > 0):
                                isAttr = True
                                if("attrs" in htmlArr[tagIndex]):
                                    attrIndex += 1
                                    attrValue = False
                                    attrInitChar = False
                        else:
                            if(isAttr != True):
                                if(isTagName):
                                    htmlArr[tagIndex]["tagName"] += char
                                else:
                                    if(c != 0):
                                        if(char == " "):
                                            if(html[c-1] == " " or ((tagIndex in htmlArr) != True and html[c-1] == ">")):
                                                c+=1
                                                continue
                                    textContent = True
                                    if(len(htmlArr)-1 < tagIndex):
                                        htmlArr.append({"textContent":char})
                                    else:
                                        htmlArr[tagIndex]["textContent"] += char
                            else:
                                if(char != '='):
                                    if(attrValue != True):
                                        if("attrs" in htmlArr[tagIndex]):
                                            if(len(htmlArr[tagIndex]["attrs"])-1 < attrIndex):
                                                htmlArr[tagIndex]["attrs"].append({"name":char,"value":""})
                                            else:
                                                htmlArr[tagIndex]["attrs"][attrIndex]["name"] += char
                                        else:
                                            htmlArr[tagIndex]["attrs"] = [{"name":char,"value":""}]
                                    else:
                                        if(attrInitChar == False):
                                            if(char in attrInitChars):
                                                attrInitChar = char
                                                c += 1
                                                continue
                                            else:
                                                attrInitChar = ' '
                                        if(attrInitChar == " " and html[c+1] == " "):
                                            htmlArr[tagIndex]["attrs"][attrIndex]["value"] += char
                                            attrValue = False
                                            attrInitChar = False
                                        else:
                                            if(attrInitChar != char):
                                                htmlArr[tagIndex]["attrs"][attrIndex]["value"] += char
                                            else:
                                                attrValue = False
                                                attrInitChar = False
                                else:
                                    attrValue = True
                else:
                    if(docType):
                        docType = False
                    else:
                        isTagName = False
                        isAttr = False
                        tagOpen = "<"+htmlArr[tagIndex]["tagName"]+">"
                        tagClose = "</"+htmlArr[tagIndex]["tagName"]+">"
                        childrenContent = html2json(html[c+1:lenHtml])
                        htmlArr[tagIndex]["children"] = childrenContent["content"]
                        c += (childrenContent["jump"]+len(tagClose))
                        tagIndex +=1
        else:
            if(textContent):
                textContent = False
                tagIndex += 1
            if(isComment):
                if(len(htmlArr)-1 < tagIndex):
                    htmlArr.append({"comment":{"content":char}})
                else:
                    htmlArr[tagIndex]["comment"]["content"] += char
                if(html[c+1] == "-" and html[c+2] == "-"):
                    c+= 4
                    tagIndex += 1
                    isComment = False
                    continue
        c += 1
    return htmlArr
print json.dumps(html2json('<abc async defer color="red" color=blue/><abc test="a" /> qreqrqer qererkqker <abc qerqer="qer" /> qerqe <!-- <html> <head> <title></title> </head> </html> --><!-- one comments --><!DOCTYPE html><!-- two comments --><html data-test="test" style="color:red;background:black" color="red" lang="tr-TR" abc="test"><!-- abc --><head><title>test</title><link rel="stylesheet" type="text/css" /><script></script></head><body><div>test</div><strong> test <strong><strong>qer<strong><strong>test</strong></strong></strong></strong></strong></body></html> <script>test</script>'))